import { Wedding } from "../src/entities";

export default interface WeddingService
{
    registerWedding(wedding:Wedding):Promise<Wedding>;
    retrieveAllWeddings():Promise<Wedding[]>;
    retrieveWeddingById(id:number):Promise<Wedding>;
    modifyWedding(wedding:Wedding):Promise<Wedding>; 
    removeWedding(id:number):Promise<Boolean>;
}