import { client } from "../src/connection";
import { WeddingDAO } from "../src/daos/wedding-dao";
import { WeddingDAOPostres } from "../src/daos/wedding-dao-postgres-impl";
import { Wedding } from "../src/entities";

const weddingDAO:WeddingDAO = new WeddingDAOPostres();

// Any entity object that has not been saved somewhere should have the ID of 0
// This is a standard software convention
const testWedding:Wedding = new Wedding(0, '2021-11-29', 'Susan', '1010 Poplar Ave', 4000);

test("Create a wedding", async ()=>
{
    const result:Wedding = await weddingDAO.createWedding(testWedding);
    expect(result.id).not.toBe(0); // An entity that is saved should have a non-zero ID
});

test('Get all weddings', async ()=>
{
    let wedding1:Wedding = new Wedding(0, '2021-11-1', 'Billy', '1111 S. Ave', 20000);
    let wedding2:Wedding = new Wedding(0, '2021-05-15', 'Jessica', '2222 W. Roberta', 10000);
    let wedding3:Wedding = new Wedding(0, '2021-03-19', 'John', '3333 N. Central', 35000);
    await weddingDAO.createWedding(wedding1);
    await weddingDAO.createWedding(wedding2);
    await weddingDAO.createWedding(wedding3);

    const weddings:Wedding[] = await weddingDAO.getAllWeddings();

    expect(weddings.length).toBeGreaterThanOrEqual(3);
});

test("Get wedding by Id", async ()=>
{
    let wedding:Wedding = new Wedding(0, '2021-11-29', 'Susan', '1010 Poplar Ave', 4000);
    wedding = await weddingDAO.createWedding(wedding);

    let retrievedWedding:Wedding = await weddingDAO.getWeddingById(wedding.id);
    // difficult to maintain

    expect(retrievedWedding.id).toBe(wedding.id);
});

test('update wedding by Id', async ()=>
{
    let wedding:Wedding = new Wedding(0, '2021-11-29', 'Jimmy', '1010 Poplar Ave', 4000);
    wedding = await weddingDAO.createWedding(wedding);

    // to update an object we just edit it and then pass it into a method
    wedding.name = 'Tom';
    wedding = await weddingDAO.updateWedding(wedding);

    expect(wedding.name).toBe('Tom');
});

test('delete wedding by Id', async ()=>
{
    let wedding:Wedding = new Wedding(0, '2021-11-29', 'Susan', '1010 Poplar Ave', 4000);
    wedding = await weddingDAO.createWedding(wedding);

    const result:boolean = await weddingDAO.deleteWeddingById(wedding.id);
    expect(result).toBeTruthy();
});

afterAll(async()=>
{
    client.end();
});
