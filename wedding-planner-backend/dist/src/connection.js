"use strict";
exports.__esModule = true;
exports.client = void 0;
var pg_1 = require("pg");
/**
 * Import the Client module from the node-postgres
 * collection. This is used to connect to the database.
 * Here, the variable client becomes an instance of Client.
 * This variable may be passed to any file.
 */
exports.client = new pg_1.Client(// used for creating a client object
{
    user: 'postgres',
    password: 'ngscmjevOoopsiu5',
    database: 'weddingdb',
    port: 5432,
    host: '34.133.87.165' //public IP address from GCP overview
});
exports.client.connect();
//process.env.DBPASSWORD
