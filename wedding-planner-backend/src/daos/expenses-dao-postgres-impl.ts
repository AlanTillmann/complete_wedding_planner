import { Expense } from "../entities";
import { client } from "../connection";
import { MissingResourceError } from "../error";
import { ExpenseDAO } from "./expenses-dao";

export class ExpenseDAOPostres implements ExpenseDAO
{
    async createExpense(expense:Expense): Promise<Expense> 
    {
        const sql:string = "insert into expense (reason, amount, w_id, photo) values ($1, $2, $3, $4) returning id";
        const values = [expense.reason, expense.amount, expense.wedding_id, expense.photo];
        const result = await client.query(sql, values);
        expense.id = result.rows[0].id;
        return expense;
    }

    async getAllExpenses(): Promise<Expense[]> 
    {
        const sql:string = 'select * from expense';
        const result = await client.query(sql);
        const expenses:Expense[] = [];
        for(const row of result.rows)
        {
            const expense:Expense = new Expense
            (
                row.id,
                row.reason,
                row.amount,
                row.w_id,
                row.photo
            );
            expenses.push(expense);
        }
        return expenses;
    }

    async getExpenseById(id: number): Promise<Expense> 
    {
        const sql:string = 'select * from expense where id=$1';
        const values = [id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${id} does not exist`);
        }
        const row = result.rows[0];
        const expense:Expense = new Expense
        (
            row.id,
            row.reason,
            row.amount,
            row.w_id,
            row.photo
        );
        return expense;
    }

    async updateExpense(expense:Expense): Promise<Expense> 
    {
        const sql:string = 'update expense set reason=$1, amount=$2, photo=$3 where id=$4';
        const values = [expense.reason, expense.amount, expense.photo, expense.id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${expense.id} does not exist`);
        }
        return expense;
    }

    async deleteExpenseById(id: number): Promise<boolean> 
    {
        const sql:string = 'delete from expense where id=$1';
        const values = [id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${id} does not exist`);
        }
        return true;
    }
}

