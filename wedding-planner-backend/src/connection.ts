import { Client } from "pg";

/**
 * Import the Client module from the node-postgres
 * collection. This is used to connect to the database.
 * Here, the variable client becomes an instance of Client.
 * This variable may be passed to any file.
 */

export const client = new Client( // used for creating a client object
{
    user:'postgres',
    password:'ngscmjevOoopsiu5', // you should NEVER STORE PASSWORDS IN CODE
    database:'weddingdb',
    port:5432,
    host:'34.133.87.165' //public IP address from GCP overview
})

client.connect();

//process.env.DBPASSWORD