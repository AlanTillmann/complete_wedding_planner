
export class MissingResourceError
{
    message:string;
    description:string = "This error means a resource cannot be found.";

    constructor(message:string)
    {
        this.message = message;
    }
}