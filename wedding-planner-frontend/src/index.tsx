import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Chat from './components/chat-page';
import CreateExpense from './components/expenses/create-expense';
import EditExpense from './components/expenses/edit-expenses';
import Login from './components/login-page';
import Planner from './components/planner-page';
import CreateWedding from './components/wedding/create-wedding';
import EditWedding from './components/wedding/edit-wedding';
import ViewWedding from './components/wedding/view-wedding';
import './index.css';


ReactDOM.render(
  <React.StrictMode>
    <Router>

      <Route path="/login">
        <Login/>
      </Route>

      <Route path="/chat">
        <Chat/>
      </Route>

      <Route path="/planner">
        <Planner/>
      </Route>

      <Route path="/view-wedding">
        <ViewWedding/>
      </Route>

      <Route path="/create-wedding">
        <CreateWedding/>
      </Route>

      <Route path="/create-expense">
        <CreateExpense/>
      </Route>

      <Route path="/edit-wedding">
        <EditWedding/>
      </Route>
      
      <Route path="/edit-expense">
        <EditExpense/>
      </Route>

    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

