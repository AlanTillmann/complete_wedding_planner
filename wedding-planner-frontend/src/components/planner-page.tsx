import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router";
import WeddingTable from "./wedding/wedding-table";


export default function Planner()
{
    const [weddings, setWeddings] = useState([]);
    const [times, setTimes] = useState(0);
    const history = useHistory<any>();

    async function getWeddings()
    {
        const response = await axios.get('http://localhost:3001/weddings');
        setWeddings(response.data);
    }

    if (times === 0)
    {
        getWeddings().then(); // how you call an async function without an event!
        setTimes(1);
    }

    return(<div>
        <h1>Wedding Planner</h1>
        <h3>Welcome, {history.location.state["email"]}</h3>
        <button className="btn btn-primary btn-lg" onClick={() => history.push({pathname:"/create-wedding", state:history.location.state})}>Create New Wedding</button>
        <button className="btn btn-primary btn-lg" onClick = {() => history.push({pathname:"/chat", state:history.location.state})}>Chat</button>
        <button className="btn btn-primary btn-lg" onClick = {() => history.push("/login")}>Logout</button>
        <WeddingTable weddings={weddings}></WeddingTable>
    </div>)
}

