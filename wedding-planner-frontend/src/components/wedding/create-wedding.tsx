import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";


export default function CreateWedding()
{
    const history = useHistory<any>();
    const dateInput = useRef<any>(null);
    const nameInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);

    // date, name, location, budget for wedding
    return(<div>
        <h3>Welcome, {history.location.state["email"]}</h3>
        <button className="btn btn-primary btn-lg" onClick = {() => history.push("/login")}>Logout</button><br/>
        <label htmlFor="date">Date: </label>
        <input name="date" type="date" ref={dateInput}/><br/>

        <label htmlFor="name">Name: </label>
        <input name="name" type="text" ref={nameInput} /><br/>

        <label htmlFor="location">Location: </label>
        <input name="location" type="text" ref={locationInput} /><br/>

        <label htmlFor="budget">Budget: </label>
        <input name="budget" type="number" ref={budgetInput} /><br/>

        <button className="btn btn-primary btn-lg" onClick={
            async () => 
            {
                const wedding =
                {
                    date:dateInput.current.value,
                    name:nameInput.current.value,
                    location:locationInput.current.value,
                    budget:budgetInput.current.value

                }
            await axios.post(`http://localhost:3001/weddings`, wedding);
            history.push({pathname:"/planner", state:history.location.state});
        }
        }>Create</button>
        <button className="btn btn-primary btn-lg" onClick={() => history.push({pathname:"/planner", state:history.location.state})}>Cancel</button>
    </div>)
}