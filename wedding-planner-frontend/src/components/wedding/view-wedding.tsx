import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router";
import ExpenseTable from "../expenses/expenses-table";

export default function ViewWedding()
{
    const history = useHistory<any>();
    const [expenses, setExpenses] = useState([]);
    const [totalExpenses, setTotalExpenses] = useState(0);
    const [times, setTimes] = useState(0);

    const weddingId = history.location.state["id"];
    const weddingName = history.location.state["name"];
    const weddingLocation = history.location.state["location"]
    const weddingBudget = history.location.state["budget"];

    async function edit() 
    {
        history.push({pathname:"/edit-wedding", state:history.location.state});
    }

    async function deleteWedding() 
    {
        await axios.delete(`http://localhost:3001/weddings/${weddingId}`);
        history.location.state["deleted"] = true;
        history.push({pathname:"/planner", state: history.location.state});
    }

    async function showExpenses()
    {
        const response = await axios.get(`http://localhost:3001/weddings/${weddingId}/expenses`);
        setExpenses(response.data);
        let newTotal:number = totalExpenses;
        for (const i of response.data)
        {
            newTotal += Number(i["amount"]);;
            setTotalExpenses(newTotal);
        }
    }

    if (times === 0)
    {
        showExpenses().then();
        setTimes(1);
    }
    
    return(
        <div>
            <h3>Additional Information</h3>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button className="btn btn-primary btn-lg" onClick = {() => history.push("/login")}>Logout</button>
            <p>ID: {weddingId}</p>
            <p>Name: {weddingName}</p>
            <p>Location: {weddingLocation}</p>
            <p>Budget: {weddingBudget}</p>

            <button className="btn btn-primary btn-lg" onClick={edit}>Edit</button>
            <button className="btn btn-primary btn-lg" onClick={deleteWedding}>Delete</button>
            <button className="btn btn-primary btn-lg" onClick={() => history.push({pathname:"/create-expense", state:history.location.state})}>Add Expense</button>

            <ExpenseTable expenses={expenses}></ExpenseTable>
            <p>Total Expenses: {totalExpenses}</p>
            <button className="btn btn-primary btn-lg" onClick={() => history.push({pathname:"/planner", state:{email:history.location.state["email"]}})}>Back</button>
        </div>
    );
}

