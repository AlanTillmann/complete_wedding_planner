import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";


export default function EditWedding()
{
    const history = useHistory<any>();
    const dateInput = useRef<any>(null);
    const nameInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);
    const id = history.location.state["id"];

    return(<div>
        <h3>Welcome, {history.location.state["email"]}</h3>
        <button className="btn btn-primary btn-lg" onClick = {() => history.push("/login")}>Logout</button>
        <label htmlFor="date">Date: </label>
        <input name="date" type="date" ref={dateInput} defaultValue={history.location.state["date"]}/><br/>

        <label htmlFor="name">Name: </label>
        <input name="name" type="text" ref={nameInput} defaultValue={history.location.state["name"]}/><br/>

        <label htmlFor="location">Location: </label>
        <input name="location" type="text" ref={locationInput} defaultValue={history.location.state["location"]}/><br/>

        <label htmlFor="budget">Budget: </label>
        <input name="budget" type="number" ref={budgetInput} defaultValue={history.location.state["budget"]}/><br/>

        <button className="btn btn-primary btn-lg" onClick={
            async () => 
            {
                const wedding:any =
                {
                    id:id,
                    date:dateInput.current.value,
                    name:nameInput.current.value,
                    location:locationInput.current.value,
                    budget:budgetInput.current.value
                }
            await axios.put(`http://localhost:3001/weddings/${id}`, wedding);
            wedding["email"] = history.location.state["email"];
            history.push({pathname:"/view-wedding", state:wedding});
        }
        }>Create</button>
        <button className="btn btn-primary btn-lg" onClick={() => history.push({pathname:"/view-wedding", state:history.location.state})}>Cancel</button>
    </div>)
}