import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";

const imageToBase64 = require('image-to-base64');


export default function CreateExpense()
{
    const history = useHistory<any>();

    // reason, amount, photo input
    const reasonInput = useRef<any>(null);
    const amountInput = useRef<any>(null);
    const id = history.location.state["id"];
    const photoInput = useRef<any>(null);
    
    return(<div>
        <h3>Welcome, {history.location.state["email"]}</h3>
        <button className="btn btn-primary btn-lg" onClick = {() => history.push("/login")}>Logout</button><br/>
        <label htmlFor="reason">Reason: </label>
        <input name="reason" type="text" ref={reasonInput}/><br/>

        <label htmlFor="amount">Amount: </label>
        <input name="amount" type="number" ref={amountInput} /><br/>

        <label htmlFor="photo">Photo: </label>
        <input name="photo" type="file" ref={photoInput} accept="image/png, image/jpeg"/><br/>

        <button className="btn btn-primary btn-lg" onClick={
            async () => 
            {
                const b64 = btoa(photoInput.current.value); // content
                
                const value:number = Math.round(Math.random() * (100001));

                const image =
                {
                    name:"image-file" + String(value),
                    extension:"jpg",
                    content:b64
                }

                // send the encoded file to the cloud function trigger url
                const response = await axios.post("https://us-central1-wedding-planner-tillmann.cloudfunctions.net/file-upload", image);

                const expense =
                {
                    reason:reasonInput.current.value,
                    amount:amountInput.current.value,
                    photo:response.data["photoLink"]
                };
            await axios.post(`http://localhost:3001/weddings/${id}/expenses`, expense);
            history.push({pathname:"/view-wedding", state:history.location.state});
        }
        }>Submit</button>
        <button className="btn btn-primary btn-lg" onClick={() => history.push({pathname:"/view-wedding", state:history.location.state})}>Cancel</button>
    </div>)
}

