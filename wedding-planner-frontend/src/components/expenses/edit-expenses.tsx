import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";
const imageToBase64 = require('image-to-base64');

export default function EditExpense()
{
    const history = useHistory<any>();

    // reason, amount
    const reasonInput = useRef<any>(null);
    const amountInput = useRef<any>(null);
    const id = history.location.state["id"];
    const w_id = history.location.state["wedding_id"];
    const photoInput = useRef<any>(null);
    
    return(<div>
        <h3>Welcome, {history.location.state["email"]}</h3>
        <button className="btn btn-primary btn-lg" onClick = {() => history.push("/login")}>Logout</button><br/>
        <label htmlFor="reason" >Reason: </label>
        <input name="reason" type="text" ref={reasonInput} defaultValue={history.location.state["reason"]} /><br/>

        <label htmlFor="amount">Amount: </label>
        <input name="amount" type="number" ref={amountInput} defaultValue={history.location.state["amount"]} /><br/>

        <label htmlFor="photo">Choose a picture: </label>
        <input name="photo" type="file" ref={photoInput} accept="image/png, image/jpeg" /><br/>

        <button className="btn btn-primary btn-lg" onClick={
            async () => 
            {
                const b64 = imageToBase64(`./pictures-for-upload/${photoInput}`).then();

                // use btoa to get the chosen file encoded into base64
                // const b64 = btoa(photoInput.current.value); // content
                
                const value:number = Math.round(Math.random() * (100001));

                const image =
                {
                    name:"image-file" + String(value),
                    extension:"jpg",
                    content:b64
                }

                // send the encoded file to the cloud function trigger url
                const response = await axios.post("http://localhost:8080/", image);

                // store the returned value in the expense object
                const expense =
                {
                    id:id,
                    reason:reasonInput.current.value,
                    amount:amountInput.current.value,
                    w_id:w_id,
                    photo:response.data["photoLink"]
                };
            await axios.put(`http://localhost:3001/expenses/${id}`, expense);
            const wedding = await axios.get(`http://localhost:3001/weddings/${w_id}`);
            wedding.data["email"] = history.location.state["email"];
            history.push({pathname:"/view-wedding", state:wedding.data});
        }
        }>Submit</button>
        <button className="btn btn-primary btn-lg" onClick={
        async () =>
        {
            const wedding = await axios.get(`http://localhost:3001/weddings/${w_id}`);
            wedding.data["email"] = history.location.state["email"];
            history.push({pathname:"/view-wedding", state:wedding.data});
        }}>Cancel</button>
    </div>)
}


