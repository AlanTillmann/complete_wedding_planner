import axios from "axios";
import { useHistory } from "react-router";

export default function ExpenseTable(props:any)
{
    const expenses = props.expenses;
    const history = useHistory<any>();

    return(<table className="table table-striped table-hover">
        <thead>
            <th>ID</th>
            <th>Reason</th>
            <th>Amount</th>
            <th>Photo</th>
        </thead>
        <tbody>
            {expenses.map((e:any) => 
            <tr key={e.id}>
                <td>{e.id}</td>
                <td>{e.reason}</td>
                <td>{e.amount}</td>
                <td>{e.photo}</td>
                <td><button onClick={() => 
                        {
                            e["email"] = history.location.state["email"];
                            history.push({pathname:"/edit-expense", state: e});
                        }
                        }>Edit</button></td>
                <td><button onClick={async () => 
                        {
                            await axios.delete(`http://localhost:3001/expenses/${e.id}`);
                            window.location.reload();
                        }
                        }>Delete</button></td>
            </tr>)}
        </tbody>
    </table>)
}

