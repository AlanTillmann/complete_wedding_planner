import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";


export default function Login()
{
    const history = useHistory<any>();
    const emailInput = useRef<any>(null);
    const passwordInput = useRef<any>(null);
    
    return(
        <>
            <h1>Login</h1><br/>

            <label htmlFor="email">Email: </label>
            <input name="email" type="text" ref={emailInput} /><br/>

            <label htmlFor="password">Password: </label>
            <input name="password" type="password" ref={passwordInput} /><br/>

            <button className="btn btn-primary btn-lg" onClick=
            {
                async () => 
                {
                    const employee =
                    {
                        email:emailInput.current.value,
                        password:passwordInput.current.value
                    }

                    try
                    {
                        await axios.patch(`https://localhost:3002/users/login`, employee);
                        history.push({pathname:"/planner", state:{email:employee.email}});
                    }
                    catch
                    {
                        alert("Invalid email or password.");
                    }
                }
            }>Login</button>
        </>
    );
}