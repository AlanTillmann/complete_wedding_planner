# Wedding Planner

## Project Description

The Wedding Planner app is an online planner for a small wedding planner company. Employees to create and manage weddings. Employees can also send each other messages. It is a micro-services cloud deployed application. 

## Technologies Used

* Google Compute Engine
* Datastore
* Cloud SQL
* App Engine
* Cloud Functions
* Cloud Storage
* Firebase

## Features

List of features ready and TODOs for future development
* The messaging service is complete and updates messages in real time.
* The wedding planner service allows for the creation and editing of weddings and expenses tied to those weddings.
* The authorization service allows for users to login and message each other.

## Getting Started
   
1. Use git clone link from GitLab repository
2. Deploy on GCP
3. Allow unsecured content on your web browser.
4. Use command "npm start" in command prompt if running locally
5. Use firebase link if project is fully deployed and hosted

## Usage

1. Use a username and password in the database to login.
2. From the main menu a wedding can be created or selected.
    - If a wedding is selected, that wedding's information will be displayed and can be edited or deleted.
3. The messaging service can also be accessed from the main menu.
    - Here you can message other users within the database and the conversation will be displayed. These messages are persisted in datastore.
4. Lastly there is a logout button available on each page to exit out of the application to the login page.

